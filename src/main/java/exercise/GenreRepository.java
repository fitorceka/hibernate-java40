package exercise;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class GenreRepository {

    private SessionFactory sessionFactory;

    public GenreRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveGenre(Genre genre) {
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(genre);
            transaction.commit();
        }
    }

    public void deleteGenre(Genre genre) {
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(genre);
            transaction.commit();
        }
    }

    public Optional<Genre> findByName(String name) {
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query<Genre> query = session.createQuery("select G from Genre G where G.name = :n", Genre.class);
            query.setParameter("n", name);
            transaction.commit();
            return Optional.ofNullable(query.getSingleResult());
        }
    }

    public Optional<Genre> findById(int id) {
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            transaction.commit();

            return Optional.ofNullable(session.find(Genre.class, id));
        }
    }

    public List<Genre> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query<Genre> query = session.createQuery("select G from Genre G", Genre.class);
            transaction.commit();
            return query.getResultList();
        }
    }
}
