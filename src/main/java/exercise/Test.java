package exercise;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Optional;

public class Test {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration().
                configure("hibernate.cfg.xml")
                .buildSessionFactory();

        GenreRepository genreRepository = new GenreRepository(sf);

        Genre genre = new Genre("Action");
//        genreRepository.saveGenre(genre);

//        Optional<Genre> find = genreRepository.findByName("Action");

        Optional<Genre> find = genreRepository.findById(1);
        find.ifPresent(System.out::println);

//        genreRepository.deleteGenre(find.get());

        //detyre shtepie
        // ndertorni ActorRepository adhe MovieRepository
        // dhe testojini ktu ne main
    }
}
