package entity;

import jakarta.persistence.Embeddable;

@Embeddable
public class SchoolId {

    private int num;
    private String letters;

    public SchoolId() {
    }

    public SchoolId(int num, String letters) {
        this.num = num;
        this.letters = letters;
    }

    @Override
    public String toString() {
        return "SchoolId{" +
                "num=" + num +
                ", letters='" + letters + '\'' +
                '}';
    }
}
