package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class Address {

    @Column(name = "STREET")
    private String street;

    @Column(name = "BUILDING_NO")
    private int buildingNo;

    @Column(name = "ZIP_CODE")
    private int zipCode;

    public Address() {
    }

    public Address(String street, int buildingNo, int zipCode) {
        this.street = street;
        this.buildingNo = buildingNo;
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuildingNo() {
        return buildingNo;
    }

    public void setBuildingNo(int buildingNo) {
        this.buildingNo = buildingNo;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", buildingNo=" + buildingNo +
                ", zipCode=" + zipCode +
                '}';
    }
}
