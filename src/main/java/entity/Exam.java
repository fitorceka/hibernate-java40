package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.util.List;

@Entity
@Table(name = "exam_info")
public class Exam extends BaseEntity {

    @Column(name = "EXAM_NAME", length = 100)
    private String name;
    private String description;

    @ManyToMany
    @JoinTable(name = "exam_student",
               joinColumns = @JoinColumn(name = "exam_id"),
                inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student> students;

    public Exam() {
    }

    public Exam(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "id='" + getId() + '\'' +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", students=" + students.size() +
                '}';
    }
}
