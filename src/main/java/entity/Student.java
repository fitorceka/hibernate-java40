package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "student_info")
public class Student extends BaseEntity {

//    @Id
//    @Column(name = "STUDENT_ID")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;

    @Column(name = "FIRST_NAME", nullable = false, length = 100)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, length = 100)
    private String lastName;

    @Column(name = "BIRTHDAY", nullable = false)
    private LocalDate birthday;

    @Column(name = "EMAIL", nullable = false, unique = true, length = 100)
    private String email;

    @Embedded
    private Address address;

    @OneToOne(mappedBy = "student")
    private StudentExtra studentExtra;

    @ManyToOne
    @JoinColumn(name = "SCHOOL_ID")
    private School school;

    @ManyToMany(mappedBy = "students")
    private List<Exam> exams;

    public Student() {
    }

//    public Student(int id, String firstName, String lastName, LocalDate birthday, String email, Address address) {
//        this.id = id;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.birthday = birthday;
//        this.email = email;
//        this.address = address;
//    }

    public Student(String firstName, String lastName, LocalDate birthday, String email, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
    }

//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public StudentExtra getStudentExtra() {
        return studentExtra;
    }

    public void setStudentExtra(StudentExtra studentExtra) {
        this.studentExtra = studentExtra;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + getId() + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", studentExtra=" + studentExtra +
                ", school=" + school +
                ", exams=" + exams +
                '}';
    }
}
