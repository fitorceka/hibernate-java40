import entity.Address;
import entity.School;
import entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;
import java.util.List;

public class TestConnection {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration().
                configure("hibernate.cfg.xml")
//                .addAnnotatedClass(Student.class) //this is like the configuration file
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();

        // per te rujatur te dhena

        Address address = new Address("Rruga Dibres", 78, 1001);
//        Student student = new Student(2, "Edi", "Rama", LocalDate.of(2000, 10, 10), "edi2.rama@ertv.com", address);

//        session.persist(student);

        //per te gjet te dhena

//        Student found = session.find(Student.class, 1);
//        System.out.println("Found student ---------------->");
//        System.out.println(found);

        // per te updatuar te dhna

//        found.setBirthday(LocalDate.of(1995, 10, 10));
//        found.getAddress().setBuildingNo(90);
//
//        session.merge(found);
//
//        System.out.println("Pas updatetimit -------------->");
//        Student afterUpdate = session.find(Student.class, 1);
//        System.out.println(afterUpdate);

        // per te fshire te dhena
//        session.remove(found);




        ////////////////////////////////////////
        //kjo eshte per id e perbere
//
//        SchoolId id = new SchoolId(1, "Tirane");
//        School school = new School(id, "Servetja", "Shkoll 9 vejecare");
//        session.persist(school);

        // duke perdorur id e gjeneruar

//        Student s = new Student("Edi", "Rama", LocalDate.of(2000, 10, 10), "edi12.rama@ertv.com", address);
//        session.persist(s);

        // lidhja 1 me 1
        Student s = new Student("Edi", "Rama", LocalDate.of(2000, 10, 10), "edi.rama@ertv.com", address);
//        session.persist(s);
//
//        Student f = session.find(Student.class, 1);
//
//        StudentExtra se = new StudentExtra();
//        se.setFather("Kristaq");
//        se.setMother("Ana");
//        se.setBirthplace("Surrel");
//        se.setStudent(f);
//
//        session.persist(se);


        // lidhja 1 me shum ose shum e 1

//        School school = new School("Servetja", "Shkoll 9 vjecare");
//
//        session.persist(school);
//
//        Student f = session.find(Student.class, 1);
//        f.setSchool(school);
//        session.merge(f);

//        School foundSchool = session.find(School.class, 1);
        Student newS = new Student("Taulant", "Balla", LocalDate.of(2000, 10, 10), "tao.tao@ertv.com", address);
//        newS.setSchool(foundSchool);
//        session.persist(newS);

        // this used for git


        // lidhja many to many

        Student s1 = new Student("Edi", "Rama", LocalDate.of(2000, 1, 10), "edi.rama@ertv.com", address);
        Student s2 = new Student("Sali", "Berisha", LocalDate.of(2001, 11, 10), "sali.b@ertv.com", address);
        Student s3 = new Student("Lali", "Eri", LocalDate.of(1998, 10, 2), "lalilali@ertv.com", address);
        Student s4 = new Student("Ilir", "Meta", LocalDate.of(2002, 12, 10), "iliri@ertv.com", address);
//
        School school = new School("Servetja", "Shkoll 9-vjecare");
//        s1.setSchool(school);
//        s2.setSchool(school);
//        s3.setSchool(school);
//        s4.setSchool(school);
//
//        session.persist(school);
//
//        Exam e1 = new Exam("Math", "Provimi matematikes");
//        Exam e2 = new Exam("Biology", "Provimi biologjis");
//        Exam e3 = new Exam("Fizike", "Provimi fizikes");
//        Exam e4 = new Exam("Java", "Provimi javas");
//
//        session.persist(s1);
//        session.persist(s2);
//        session.persist(s3);
//        session.persist(s4);
//
//        e1.setStudents(List.of(s2, s4));
//        e2.setStudents(List.of(s1, s2, s4));
//        e3.setStudents(List.of(s1, s2, s3, s4));
//
//        session.persist(e1);
//        session.persist(e2);
//        session.persist(e3);
//        session.persist(e4);

        // testojme find
//        Student gjej = session.find(Student.class, 4);
//
//        System.out.println(gjej);


        // ruaj entittet pa perdorur sesion.persist

        school.setStudents(List.of(s1, s2, s3, s4));
        s1.setSchool(school);
        s2.setSchool(school);
        s3.setSchool(school);
        s4.setSchool(school);
        session.persist(school);


        transaction.commit();
    }
}
