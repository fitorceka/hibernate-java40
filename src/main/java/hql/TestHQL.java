package hql;

import entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class TestHQL {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration().
                configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();

        Transaction transaction = session.beginTransaction();
//
         //find alll
//        Query<Student> query = session.createQuery("from Student", Student.class);
//
//        List<Student> getAll = query.getResultList();
//
//        System.out.println(getAll);


        // find by id by parameter
//        Query<Student> query = session.createQuery("select S from Student S where S.id = :sId", Student.class);
//
        Scanner scanner = new Scanner(System.in);
//
//        System.out.print("Shruaj id e studentit qe do te gjesh: ");
//        int id = scanner.nextInt();
//
//        query.setParameter("sId", id);
//
//        Student get = query.getSingleResult();
//
//        System.out.println(get);

        // gjej gjitha dhe bej order

//        Query<Student> query = session.createQuery("select S from Student S order by S.firstName desc", Student.class);
//
//        List<Student> getAll = query.getResultList();
//
//        System.out.println(getAll);

        // kusht mre kompleks

//        Query<Student> query = session.createQuery("select S from Student S where " +
//                "S.studentExtra.birthplace = :city and S.birthday > :b", Student.class);
//
//        System.out.print("Enter city: ");
//        String city = scanner.nextLine();
//
//        System.out.print("Enter b: ");
//        LocalDate b = LocalDate.parse(scanner.nextLine());
//
//        query.setParameter("city", city);
//        query.setParameter("b", b);
//
//        List<Student> getAll = query.getResultList();
//
//        System.out.println(getAll);


        Query<Student> query = session.createQuery("select S from Student S where " +
                "(S.studentExtra.birthplace = :c1 or S.studentExtra.birthplace = :c2) and S.birthday > :b", Student.class);

        System.out.print("Enter city1: ");
        String c1 = scanner.nextLine();

        System.out.print("Enter city2: ");
        String c2 = scanner.nextLine();

        System.out.print("Enter b: ");
        LocalDate b = LocalDate.parse(scanner.nextLine());

        query.setParameter("c1", c1);
        query.setParameter("c2", c2);
        query.setParameter("b", b);

        List<Student> getAll = query.getResultList();

        System.out.println(getAll);

        transaction.commit();
    }
}
